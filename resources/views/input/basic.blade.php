@extends('layouts.input')

@section('form')
@include('component.remaja-basic-edit', [
    'remaja' => $input, 
    'submitText' => 'Next',
    'actionUrl' => route('input.basic.post'),
])
@endsection
