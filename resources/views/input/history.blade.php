@extends('layouts.input')

@section('form')
@include('component.remaja-history-edit', [
    'remaja' => $input, 
    'submitText' => 'Next',
    'actionUrl' => route('input.history.post'),
])
@endsection
