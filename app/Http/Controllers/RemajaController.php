<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remaja;
use App\Zone;
use App\Kumpulan;
use App\Photo;

class RemajaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($id, Request $request)
    {
        $section = $request->input('section', 'basic');
        return view('remaja.view', [
            'section' => $section,
            'remaja' => Remaja::find($id),
        ]);
    }
    
    public function showEdit($id, Request $request)
    {
        $section = $request->input('section', 'basic');
        $remaja = Remaja::find($id);
        $zones = Zone::all();
        $kumpulans = Kumpulan::all();
        return view('remaja.edit', [
            'section' => $section,
            'remaja' => $remaja,
            'zones' => $zones,
            'kumpulans' => $kumpulans,
            'actionUrl' => route('remaja.edit', [
                'id' => $remaja->id,
                'section' => $section,
            ])
        ]);
    }

    public function showDelete($id)
    {
        return view('remaja.delete', [
            'remaja' => Remaja::find($id),
        ]);
    }

    public function doEdit($id, Request $request)
    {
        $remaja = Remaja::find($id);

        $photo = $request->file('photo');
        if (isset($photo)) {
            $path = $photo->store('photos');

            $photoData = new Photo();
            $photoData->remaja_id = $remaja->id;
            $photoData->path = $path;
            $photoData->save();
            return redirect()->route('remaja.edit', [
                'id' => $id,
                'section' => 'photo',
            ]);
        }

        $input = $request->all();
        $remaja->fill((array)$input);
        $remaja->save();
        return redirect()->route('remaja.view', [
            'id' => $id,
            'section' => $input['section'],
        ]);
    }

    public function doDeletePhoto($id, Request $request)
    {
        $path = $request->input('path');
        $photo = Photo::where('path', $path)->delete();

        return redirect()->route('remaja.edit', [
            'id' => $id,
            'section' => 'photo',
        ]);
    }

    public function doDelete($id)
    {
        Remaja::destroy($id);
        return redirect()->route('home');
    }
}
