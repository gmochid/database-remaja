@php
    $zones = App\Zone::all();
    $selectedZone = null;
    if($remaja->zone_id != null) {
        foreach($zones as $zone) {
            if($zone->id == $remaja->zone_id) {
                $selectedZone = $zone;
            }
        }
    }
@endphp

<h4 class="ui header">Biodata Diri</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td class="six wide active">Zon</td>
    <td class="ten wide">{{ @$remaja->zone->name ?: (@$selectedZone ? $selectedZone->name : '') }}</td>
    </tr>
    <tr>
    <td class="six wide active">Nama Penuh</td>
    <td class="ten wide">{{ @$remaja->original_name ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Nama Hijrah</td>
    <td class="ten wide">{{ @$remaja->fullname ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Jenis Kelamin / Jantina</td>
    <td class="ten wide">{{ @$remaja->jantina === 'L' ? 'Laki-laki' : (@$remaja->jantina === 'P' ? 'Perempuan' : '') }}</td>
    </tr>
    <tr>
    <td class="six wide active">No. KP / Passport</td>
    <td class="ten wide">{{ @$remaja->residence_number ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Umur</td>
    <td class="ten wide">{{ @$remaja->age ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Kategori Umur</td>
    <td class="ten wide">{{ @$remaja->age_category ? array("", "Sekolah Menengah", "Vokasional", "Pra-Staf", "Staf")[$remaja->age_category] : '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Tempat Sekolah</td>
    <td class="ten wide">{{ @$remaja->school ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Status Perkahwinan</td>
    <td class="ten wide">{{ @$remaja->marital_status ? array("", "Bujang", "Dara", "Mono", "Poli", "Duda", "Balu")[$remaja->marital_status] : '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Jumlah adik-beradik dari Se-ibu</td>
    <td class="ten wide">{{ @$remaja->brothers_from_mom ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Jumlah adik-beradik dari Semua Ibu</td>
    <td class="ten wide">{{ @$remaja->brothers_from_all_mom ?: '' }}</td>
    </tr>
</tbody>
</table>