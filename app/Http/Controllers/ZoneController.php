<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;

class ZoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show zone list
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('zone.index', [
            'zones' => Zone::all(),
        ]);
    }

    public function showCreate()
    {
        return view('zone.create');
    }

    public function showEdit($id)
    {
        return view('zone.edit', [
            'zone' => Zone::find($id),
        ]);
    }

    public function showDelete($id)
    {
        return view('zone.delete', [
            'zone' => Zone::find($id),
        ]);
    }

    public function doCreate(Request $request)
    {
        $input = $request->all();
        $zone = new Zone();
        $zone->fill((array)$input);
        $zone->save();

        return redirect('zone');
    }

    public function doEdit(Request $request)
    {
        $input = $request->all();
        $zone = Zone::find($id);
        $zone->fill((array)$input);
        $zone->save();

        return redirect('zone');
    }

    public function doDelete(Request $request)
    {
        Remaja::destroy($id);
        return redirect('zone');
    }
}
