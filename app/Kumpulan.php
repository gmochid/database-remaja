<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kumpulan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kumpulan';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_token'];

    public function bani()
    {
        return $this->belongsTo('App\Bani');
    }

    public function remaja()
    {
        return $this->hasMany('App\Remaja');
    }
}
