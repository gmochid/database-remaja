<h4 class="ui header">Foto</h4>
@if(isset($remaja->photos) && count($remaja->photos) > 0)
  <div class="ui cards">
    @foreach ($remaja->photos as $photo)
      <div class="card">
        <div class="image">
          @if (is_string($photo))
            <a href="/storage/app/{{ $photo }}">
              <img src="/storage/app/{{ $photo }}" style="max-width: 320px">
            </a>
          @else
            <a href="/storage/app/{{ $photo->path }}">
              <img src="/storage/app/{{ $photo->path }}" style="max-width: 320px">
            </a>
          @endif
        </div>
      </div>
    @endforeach
  </div>
@else
  <div>Belum ada foto</div>
@endif
