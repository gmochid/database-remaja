<form class="ui form" action="{{ $actionUrl }}" method="post">
    {{ csrf_field() }} 
    <table class="ui celled table">
        <thead>
            <tr>
            <th class="one wide">No</th>
            <th class="thirteen wide">Jenayah / Maksiat / Salah Laku</th>
            <th class="two wide"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>1</td>
            <td>Merokok</td>
            <td>
                <input type="checkbox" name="jenayah1" {{@$remaja->jenayah1 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>2</td>
            <td>Dadah</td>
            <td>
                <input type="checkbox" name="jenayah2" {{@$remaja->jenayah2 ? 'checked' : ''}}>
            </td>
            </tr>

            <tr>
            <td>3</td>
            <td colspan="2">Mencuri</td>
            </tr>

            <tr>
            <td></td>
            <td>3a. Mencuri barangan ahli jamaah / di premis GISB</td>
            <td>
                <input type="checkbox" name="jenayah3" {{@$remaja->jenayah3 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td></td>
            <td>3b. Mencuri barangan orang luar jamaah / di premis luar</td>
            <td>
                <input type="checkbox" name="jenayah4" {{@$remaja->jenayah4 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>4</td>
            <td>Cyber cafe / bermain game</td>
            <td>
                <input type="checkbox" name="jenayah5" {{@$remaja->jenayah5 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>5</td>
            <td>Menonton / melayari video lucah di TV atau HP</td>
            <td>
                <input type="checkbox" name="jenayah6" {{@$remaja->jenayah6 ? 'checked' : ''}}>
            </td>
            </tr>

            <tr>
            <td>6</td>
            <td colspan="2">Menonton Filem-filem di TV / HP</td>
            </tr>
            
            <tr>
            <td></td>
            <td>6a. Film dalam negara</td>
            <td>
                <input type="checkbox" name="jenayah7" {{@$remaja->jenayah7 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td></td>
            <td>6b. Film luar negara</td>
            <td>
                <input type="checkbox" name="jenayah8" {{@$remaja->jenayah8 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>7</td>
            <td colspan="2">Menonton Filem-filem di TV / HP</td>
            </tr>

            <tr>
            <td></td>
            <td>7a. Lagu-lagu melayu, lagu cinta, dan lagu lain</td>
            <td>
                <input type="checkbox" name="jenayah9" {{@$remaja->jenayah9 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td></td>
            <td>7b. Lagu-lagu bukan melayu</td>
            <td>
                <input type="checkbox" name="jenayah10" {{@$remaja->jenayah10 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>8</td>
            <td>Tengok bola di TV / HP</td>
            <td>
                <input type="checkbox" name="jenayah11" {{@$remaja->jenayah11 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>9</td>
            <td>Menggunakan Social Media (Facebook, Twitter dan sejenis)</td>
            <td>
                <input type="checkbox" name="jenayah12" {{@$remaja->jenayah12 ? 'checked' : ''}}>
            </td>
            </tr>

            <tr>
            <td>10</td>
            <td colspan="2">Berhubung dengan lawan jantina</td>
            </tr>
            
            <tr>
            <td></td>
            <td>10a. Berjumpa / dating</td>
            <td>
                <input type="checkbox" name="jenayah13" {{@$remaja->jenayah13 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td></td>
            <td>10b. Berhubung dengan HP</td>
            <td>
                <input type="checkbox" name="jenayah14" {{@$remaja->jenayah14 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>11</td>
            <td>Liwat</td>
            <td>
                <input type="checkbox" name="jenayah15" {{@$remaja->jenayah15 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>12</td>
            <td>Lesbian</td>
            <td>
                <input type="checkbox" name="jenayah16" {{@$remaja->jenayah16 ? 'checked' : ''}}>
            </td>
            </tr>
            
            <tr>
            <td>13</td>
            <td>Zina</td>
            <td>
                <input type="checkbox" name="jenayah17" {{@$remaja->jenayah17 ? 'checked' : ''}}>
            </td>
            </tr>
        </tbody>
    </table>
    <button type="submit" class="ui primary button">{{$submitText}}</button>
</form>
