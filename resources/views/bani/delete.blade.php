@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <a href="{{route('bani')}}" class="section">List Bani</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">Hapus Bani</div>
                </div>
            </div>
            <div class="ui form" style="margin-top: 16px;">
                <form method="post" action={{route('bani.delete.post', ['id' => $bani->id])}}>
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Id</label>
                        <input type="text" name="name" placeholder="Nama Bani" value="{{ $bani->id }}" disabled/>
                    </div>
                    <div class="field">
                        <label>Nama Bani</label>
                        <input type="text" name="name" placeholder="Nama Bani" value="{{ $bani->name }}" disabled/>
                    </div>
                    <button type="submit" class="ui negative button">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            name: 'empty',
        }
    });
});
</script>
@endpush
