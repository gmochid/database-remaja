<h4 class="ui header">Maklumat Bapa</h4>
<table class="ui olive table">
<tbody>
    <tr>
    <td class="six wide active">Nama Bapa</td>
    <td class="ten wide">{{ @$remaja->father_name ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">No. KP / Passport</td>
    <td class="ten wide">{{ @$remaja->father_residence_number ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Ahli GISBH</td>
    <td class="ten wide">{{ @$remaja->father_gisb_member ? 'Ya' : 'Tidak' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Zon / Jawatan / Tugas</td>
    <td class="ten wide">{{ @$remaja->father_zone_position ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Status Perkahwinan</td>
    <td class="ten wide">{{ @$remaja->father_marital_status ? array("", "Mono", "Poli")[$remaja->father_marital_status] : '' }}</td>
    </tr>
</tbody>
</table>

<h4 class="ui header">Maklumat Ibu</h4>
<table class="ui purple table">
<tbody>
    <tr>
    <td class="six wide active">Nama Bapa</td>
    <td class="ten wide">{{ @$remaja->mother_name ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">No. KP / Passport</td>
    <td class="ten wide">{{ @$remaja->mother_residence_number ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Ahli GISBH</td>
    <td class="ten wide">{{ @$remaja->mother_gisb_member ? 'Ya' : 'Tidak' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Zon / Jawatan / Tugas</td>
    <td class="ten wide">{{ @$remaja->mother_zone_position ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Status Perkahwinan</td>
    <td class="ten wide">{{ @$remaja->mother_marital_status ? array("", "Mono", "Poli", "Balu", "Janda")[$remaja->mother_marital_status] : '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Isteri ke</td>
    <td class="ten wide">{{ @$remaja->mother_wive_number ?: '' }}</td>
    </tr>
</tbody>
</table>
