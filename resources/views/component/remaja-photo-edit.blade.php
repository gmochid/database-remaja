<h4 class="ui header">Foto</h4>

<?php
    // var_dump($remaja->photos[0]);
    // die();
?>

<div class="ui cards">
    @foreach ($remaja->photos as $photo)
        <div class="card">
            <div class="image">
                @if (is_string($photo))
                    <img src="/storage/app/{{ $photo }}">
                @else
                    <img src="/storage/app/{{ $photo->path }}">
                @endif
            </div>
            <div class="extra content">
                <form action="{{ $actionDeleteUrl }}" method="post">
                    {{ csrf_field() }}
                    @if (is_string($photo))
                        <input type="hidden" name="path" value="{{$photo}}" />
                    @else
                        <input type="hidden" name="path" value="{{$photo->path}}" />
                    @endif
                    <button type="submit" class="ui basic red button">Hapus</button>
                </form>
            </div>
        </div>
    @endforeach
</div>

<hr />

<form class="ui form" action="{{ $actionUrl }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="fields">
        <div class="field">
            <label>Upload Foto</label>
            <input type="file" name="photo"/>
        </div>
    </div>
    <button type="submit" class="ui primary button">{{$submitText}}</button>
</form>

@push('js')
    <script>
        $(document).ready(function(){
            $('.ui.form').form({
                fields: {
                    photo: 'empty',
                }
            });
        });
    </script>
@endpush
