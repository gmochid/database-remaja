<?php

use Illuminate\Database\Seeder;

class KumpulanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bani')->insert([
            [
                'id' => 1, 
                'name' => 'Bani Abbasiah',
            ], [
                'id' => 2, 
                'name' => 'Bani Umaiyah',
            ],
        ]);

        DB::table('kumpulan')->insert([
            [
                'id' => 1, 
                'bani_id' => 1,
                'name' => 'Kumpulan Saidina Muawwiyah',
            ], [
                'id' => 2, 
                'bani_id' => 1,
                'name' => 'Kumpulan Thariq Ziyad',
            ], [
                'id' => 3,
                'bani_id' => 1,
                'name' => 'Kumpulan Uwais Al Qarni',
            ], [
                'id' => 4,
                'bani_id' => 1,
                'name' => 'Kumpulan Imam Hasan Al Basri',
            ], [
                'id' => 5, 
                'bani_id' => 2,
                'name' => 'Kumpulan Syeikh Shamsudin Al Wali',
            ], [
                'id' => 6, 
                'bani_id' => 2,
                'name' => 'Kumpulan Sultan Muhammad Al Fateh',
            ], [
                'id' => 7,
                'bani_id' => 2,
                'name' => 'Kumpulan Syeikh Hj Bayram',
            ], [
                'id' => 8,
                'bani_id' => 2,
                'name' => 'Kumpulan Hasan Ulubatle',
            ],
        ]);
    }
}
