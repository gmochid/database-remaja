@extends('layouts.input')

@section('form')
    @include('component.remaja-photo-edit', [
        'remaja' => $input,
        'submitText' => 'Next',
        'actionUrl' => route('input.photo.post'),
        'actionDeleteUrl' => route('input.photo.delete.post'),
    ])
@endsection
