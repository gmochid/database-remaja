<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bani;

class BaniController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show bani list
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bani.index', [
            'banis' => Bani::all(),
        ]);
    }

    public function showCreate()
    {
        return view('bani.create');
    }

    public function showEdit($id)
    {
        return view('bani.edit', [
            'bani' => Bani::find($id),
        ]);
    }

    public function showDelete($id)
    {
        return view('bani.delete', [
            'bani' => Bani::find($id),
        ]);
    }

    public function doCreate(Request $request)
    {
        $input = $request->all();
        $bani = new Bani();
        $bani->fill((array)$input);
        $bani->save();

        return redirect('bani');
    }

    public function doEdit(Request $request)
    {
        $input = $request->all();
        $bani = Bani::find($id);
        $bani->fill((array)$input);
        $bani->save();

        return redirect('bani');
    }

    public function doDelete(Request $request)
    {
        Remaja::destroy($id);
        return redirect('bani');
    }
}
