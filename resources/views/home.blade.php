@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <form method="get" action="{{route('home')}}">
                <div class="ui icon input">
                    <input type="text" name="search" placeholder="Search..." value="{{ @$search ?: '' }}">
                    <i class="search icon"></i>
                </div>
                <select name="zoneId" class="ui dropdown">
                    <option value="">Zon</option>
                    @foreach($zones as $zone)
                        <option {{ @$zoneId == $zone->id ? 'selected' : '' }} value="{{ $zone->id }}">{{$zone->name}}</option>
                    @endforeach
                </select>
                <button type="submit" class="ui primary button">Search</button>
            </form>
            <table class="ui single line table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>
                            @include('component.table-title-sortable', [
                                'orderBy' => $orderBy,
                                'order' => $order,
                                'search' => $search,
                                'zoneId' => $zoneId,
                                'name' => 'fullname',
                                'label' => 'Nama',
                            ])
                        </th>
                        <th>
                            @include('component.table-title-sortable', [
                                'orderBy' => $orderBy,
                                'order' => $order,
                                'search' => $search,
                                'zoneId' => $zoneId,
                                'name' => 'zone_id',
                                'label' => 'Zon',
                            ])
                        </th>
                        <th>
                            @include('component.table-title-sortable', [
                                'orderBy' => $orderBy,
                                'order' => $order,
                                'search' => $search,
                                'zoneId' => $zoneId,
                                'name' => 'kumpulan_id',
                                'label' => 'Kumpulan',
                            ])
                        </th>
                        <th>
                            @include('component.table-title-sortable', [
                                'orderBy' => $orderBy,
                                'order' => $order,
                                'search' => $search,
                                'zoneId' => $zoneId,
                                'name' => 'jantina',
                                'label' => 'Jenis Kelamin',
                            ])
                        </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($remajas as $remaja)
                        <tr>
                            <td>{{ (($remajas->currentPage() - 1) * $remajas->perPage()) + $loop->iteration}}</td>
                            <td>{{$remaja->fullname}}</td>
                            <td>{{@$remaja->zone->name ?: ''}}</td>
                            <td>{{@$remaja->kumpulan->name ?: ''}}</td>
                            <td>{{$remaja->jantina}}</td>
                            <td>
                                <a href="{{route('remaja.view', ['id' => $remaja->id])}}" class="ui basic icon button">
                                    <i class="search icon"></i>
                                </a>
                                <a href="{{route('remaja.edit', ['id' => $remaja->id])}}" class="ui basic icon button">
                                    <i class="edit icon"></i>
                                </a>
                                <a href="{{route('remaja.delete', ['id' => $remaja->id])}}" class="ui negative icon button">
                                    <i class="trash icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $remajas->links() }}
        </div>
    </div>
</div>
@endsection
