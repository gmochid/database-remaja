@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
          <div class="ui items">
            <div class="item">
              <div class="content">
                <div class="header"><a href="{{ route('zone') }}">Zon</a></div>
                <div class="meta">Tambah/Edit data Zon</div>
              </div>
            </div>
            <div class="item">
              <div class="content">
                <div class="header"><a href="{{ route('bani') }}">Bani</a></div>
                <div class="meta">Tambah/Edit data Bani</div>
              </div>
            </div>
            <div class="item">
              <div class="content">
                <div class="header"><a href="{{ route('kumpulan') }}">Kumpulan</a></div>
                <div class="meta">Tambah/Edit data Kumpulan</div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
