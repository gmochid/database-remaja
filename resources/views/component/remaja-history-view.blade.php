<h4 class="ui header">Rekod Jenayah / Maksiat / Salah Laku</h4>
<table class="ui celled red table">
<thead>
    <tr>
    <td class="one wide active"><b>No</b></td>
    <td class="thirteen wide active"><b>Jenayah / Maksiat / Salah Laku</b></td>
    <td class="two wide active"></td>
    </tr>
</thead>
<tbody>
    <tr class="{{ @$remaja->jenayah1 ? 'negative' : '' }}">
    <td>1</td>
    <td>Merokok</td>
    <td>{{ @$remaja->jenayah1 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah2 ? 'negative' : '' }}">
    <td>2</td>
    <td>Dadah</td>
    <td>{{ @$remaja->jenayah2 ? 'Ya' : 'Tidak' }}</td>
    </tr>

    <tr>
    <td>3</td>
    <td colspan="2">Mencuri</td>
    </tr>

    <tr class="{{ @$remaja->jenayah3 ? 'negative' : '' }}">
    <td></td>
    <td>3a. Mencuri barangan ahli jamaah / di premis GISB</td>
    <td>{{ @$remaja->jenayah3 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah4 ? 'negative' : '' }}">
    <td></td>
    <td>3b. Mencuri barangan orang luar jamaah / di premis luar</td>
    <td>{{ @$remaja->jenayah4 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah5 ? 'negative' : '' }}">
    <td>4</td>
    <td>Cyber cafe / bermain game</td>
    <td>{{ @$remaja->jenayah5 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah6 ? 'negative' : '' }}">
    <td>5</td>
    <td>Menonton / melayari video lucah di TV atau HP</td>
    <td>{{ @$remaja->jenayah6 ? 'Ya' : 'Tidak' }}</td>
    </tr>

    <tr>
    <td>6</td>
    <td colspan="2">Menonton Filem-filem di TV / HP</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah7 ? 'negative' : '' }}">
    <td></td>
    <td>6a. Film dalam negara</td>
    <td>{{ @$remaja->jenayah7 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah8 ? 'negative' : '' }}">
    <td></td>
    <td>6b. Film luar negara</td>
    <td>{{ @$remaja->jenayah8 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr>
    <td>7</td>
    <td colspan="2">Menonton Filem-filem di TV / HP</td>
    </tr>

    <tr class="{{ @$remaja->jenayah9 ? 'negative' : '' }}">
    <td></td>
    <td>7a. Lagu-lagu melayu, lagu cinta, dan lagu lain</td>
    <td>{{ @$remaja->jenayah9 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah10 ? 'negative' : '' }}">
    <td></td>
    <td>7b. Lagu-lagu bukan melayu</td>
    <td>{{ @$remaja->jenayah10 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah11 ? 'negative' : '' }}">
    <td>8</td>
    <td>Tengok bola di TV / HP</td>
    <td>{{ @$remaja->jenayah11 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah12 ? 'negative' : '' }}">
    <td>9</td>
    <td>Menggunakan Social Media (Facebook, Twitter dan sejenis)</td>
    <td>{{ @$remaja->jenayah12 ? 'Ya' : 'Tidak' }}</td>
    </tr>

    <tr>
    <td>10</td>
    <td colspan="2">Berhubung dengan lawan jantina</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah13 ? 'negative' : '' }}">
    <td></td>
    <td>10a. Berjumpa / dating</td>
    <td>{{ @$remaja->jenayah13 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah14 ? 'negative' : '' }}">
    <td></td>
    <td>10b. Berhubung dengan HP</td>
    <td>{{ @$remaja->jenayah14 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah15 ? 'negative' : '' }}">
    <td>11</td>
    <td>Liwat</td>
    <td>{{ @$remaja->jenayah15 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah16 ? 'negative' : '' }}">
    <td>12</td>
    <td>Lesbian</td>
    <td>{{ @$remaja->jenayah16 ? 'Ya' : 'Tidak' }}</td>
    </tr>
    
    <tr class="{{ @$remaja->jenayah17 ? 'negative' : '' }}">
    <td>13</td>
    <td>Zina</td>
    <td>{{ @$remaja->jenayah17 ? 'Ya' : 'Tidak' }}</td>
    </tr>
</tbody>
</table>
