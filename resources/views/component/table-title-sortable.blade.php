@if($orderBy == $name)
    @if($order == 'asc')
        <a href="{{route('home', ['orderBy' => $name, 'order' => 'desc', 'search' => $search, 'zoneId' => $zoneId])}}">{{$label}}</a>
        <i class="angle up icon"></i>
    @else
        <a href="{{route('home', ['orderBy' => $name, 'order' => 'asc', 'search' => $search, 'zoneId' => $zoneId])}}">{{$label}}</a>
        <i class="angle down icon"></i>
    @endif
@else
    <a href="{{route('home', ['orderBy' => $name, 'order' => 'asc', 'search' => $search, 'zoneId' => $zoneId])}}">{{$label}}</a>
@endif
