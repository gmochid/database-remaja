<form class="ui form" action="{{ $actionUrl }}" method="post">
    {{ csrf_field() }} 
    <h4 class="ui header">Bapa</h4>
    <div class="fields">
        <div class="six wide field">
            <label>Nama Bapa</label>
            <input type="text" name="father_name" placeholder="Nama Bapa" value="{{@$remaja->father_name ?: ''}}">
        </div>
        <div class="six wide field">
            <label>No. KP/Passport</label>
            <input type="text" name="father_residence_number" placeholder="No. RK/Passport Bapa" value="{{@$remaja->father_residence_number ?: ''}}">
        </div>
        <div class="four wide field">
            <label>Ahli GISBH</label>
            <div class="ui checkbox">
            <input type="checkbox" name="father_gisb_member" tabindex="0" class="hidden" {{ @$remaja->father_gisb_member ? 'checked' : '' }}>
            </div>
        </div>
    </div>
    <div class="field">
        <label>Zon / Jawatan / Tugas</label>
        <input type="text" name="father_zone_position" placeholder="Zon / Jawatan / Tugas" value="{{@$remaja->father_zone_position ?: ''}}">
    </div>
    <div class="field">
        <label>Status Perkahwinan</label>
        <select name="father_marital_status" class="ui dropdown">
            <option value="">Status Perkahwinan</option>
            <option {{ (int)@$remaja->father_marital_status === 1 ? 'selected' : ''}} value="1">Mono</option>
            <option {{ (int)@$remaja->father_marital_status === 2 ? 'selected' : ''}} value="2">Poli</option>
        </select>
    </div>
    <div class="ui divider"></div>
    <h4 class="ui header">Ibu</h4>
    <div class="fields">
        <div class="six wide field">
            <label>Nama Ibu</label>
            <input type="text" name="mother_name" placeholder="Nama Bapa" value="{{@$remaja->mother_name ?: ''}}">
        </div>
        <div class="six wide field">
            <label>No. KP/Passport</label>
            <input type="text" name="mother_residence_number" placeholder="No. RK/Passport Bapa" value="{{@$remaja->mother_residence_number ?: ''}}">
        </div>
        <div class="four wide field">
            <label>Ahli GISBH</label>
            <div class="ui checkbox">
            <input type="checkbox" name="mother_gisb_member" tabindex="0" class="hidden" {{ @$remaja->mother_gisb_member ? 'checked=' : '' }}>
            </div>
        </div>
    </div>
    <div class="field">
        <label>Zon / Jawatan / Tugas</label>
        <input type="text" name="mother_zone_position" placeholder="Zon / Jawatan / Tugas" value="{{@$remaja->mother_zone_position ?: ''}}">
    </div>
    <div class="field">
        <label>Status Perkahwinan</label>
        <select name="mother_marital_status" class="ui dropdown">
            <option value="">Status Perkahwinan</option>
            <option {{ (int)@$remaja->mother_marital_status === 1 ? 'selected' : ''}} value="1">Mono</option>
            <option {{ (int)@$remaja->mother_marital_status === 2 ? 'selected' : ''}} value="2">Poli</option>
            <option {{ (int)@$remaja->mother_marital_status === 3 ? 'selected' : ''}} value="3">Balu</option>
            <option {{ (int)@$remaja->mother_marital_status === 4 ? 'selected' : ''}} value="4">Janda</option>
        </select>
    </div>
    <div class="field">
        <label>Isteri ke</label>
        <input type="text" name="mother_wive_number" placeholder="Isteri ke" value="{{@$remaja->mother_wive_number ?: ''}}">
    </div>
    <button type="submit" class="ui primary button">{{$submitText}}</button>
</form>

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            father_name: 'empty',
            father_residence_number: 'empty',
            father_zone_position: 'empty',
            father_marital_status: 'empty',
            mother_name: 'empty',
            mother_residence_number: 'empty',
            mother_zone_position: 'empty',
            mother_marital_status: 'empty',
            mother_wive_number: ['empty', 'integer'],
        }
    });
});
</script>
@endpush
