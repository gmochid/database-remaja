@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <a href="{{route('kumpulan')}}" class="section">List Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">Tambah Kumpulan</div>
                </div>
            </div>
            <div class="ui form" style="margin-top: 16px;">
                <form method="post" action={{route('kumpulan.create.post')}}>
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Nama Kumpulan</label>
                        <input type="text" name="name" placeholder="Nama Kumpulan"/>
                    </div>
                    <div class="field">
                        <label>Bani</label>
                        <select name="zone_id" class="ui dropdown">
                            <option value="">Bani</option>
                            @foreach($banis as $bani)
                                <option {{ @$kumpulan->bani->id === $bani->id ? 'selected' : ((int)@$kumpulan->bani_id === $bani->id ? 'selected' : '')}} value="{{$loop->index + 1}}">{{$bani->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="ui primary button">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            name: 'empty',
        }
    });
});
</script>
@endpush
