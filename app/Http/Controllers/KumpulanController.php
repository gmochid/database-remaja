<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kumpulan;
use App\Bani;

class KumpulanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show kumpulan list
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kumpulan.index', [
            'kumpulans' => Kumpulan::all(),
        ]);
    }

    public function showCreate()
    {
        return view('kumpulan.create', [
            'banis' => Bani::all(),
        ]);
    }

    public function showEdit($id)
    {
        return view('kumpulan.edit', [
            'kumpulan' => Kumpulan::find($id),
        ]);
    }

    public function showDelete($id)
    {
        return view('kumpulan.delete', [
            'kumpulan' => Kumpulan::find($id),
        ]);
    }

    public function doCreate(Request $request)
    {
        $input = $request->all();
        $kumpulan = new Kumpulan();
        $kumpulan->fill((array)$input);
        $kumpulan->save();

        return redirect('kumpulan');
    }

    public function doEdit(Request $request)
    {
        $input = $request->all();
        $kumpulan = Kumpulan::find($id);
        $kumpulan->fill((array)$input);
        $kumpulan->save();

        return redirect('kumpulan');
    }

    public function doDelete(Request $request)
    {
        Remaja::destroy($id);
        return redirect('kumpulan');
    }
}
