<form class="ui form" action="{{ $actionUrl }}" method="post">
    {{ csrf_field() }} 
    <h4 class="ui header">Biodata Diri</h4>
    <div class="field">
        <label>Zon</label>
        <select name="zone_id" class="ui dropdown">
            <option value="">Zon</option>
            @foreach($zones as $zone)
                <option {{ @$remaja->zone->id === $zone->id ? 'selected' : ((int)@$remaja->zone_id === $zone->id ? 'selected' : '')}} value="{{ $zone->id }}">{{$zone->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="field">
        <label>Kumpulan</label>
        <select name="kumpulan_id" class="ui dropdown">
            <option value="">Kumpulan</option>
            @foreach($kumpulans as $kumpulan)
                <option {{ @$remaja->kumpulan->id === $kumpulan->id ? 'selected' : ((int)@$remaja->kumpulan_id === $kumpulan->id ? 'selected' : '')}} value="{{ $kumpulan->id }}">
                    {{$kumpulan->bani->name}} - {{$kumpulan->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Nama Penuh</label>
            <input type="text" name="original_name" placeholder="Nama Lengkap" value="{{@$remaja->original_name ?: ''}}">
        </div>
        <div class="field">
            <label>Nama Hijrah</label>
            <input type="text" name="fullname" placeholder="Nama Hijrah" value="{{@$remaja->fullname ?: ''}}">
        </div>
    </div>
    <div class="field">
        <label>No. KP / Passport</label>
        <input type="text" name="residence_number" placeholder="No. RK / Passport" value="{{@$remaja->residence_number ?: ''}}">
    </div>
    <div class="field">
        <label>Jenis Kelamin / Jantina</label>
        <select name="jantina" class="ui dropdown">
            <option value="">Kumpulan</option>
            <option {{ @$remaja->jantina === 'L' ? 'selected' : '' }} value="L">Laki-laki</option>
            <option {{ @$remaja->jantina === 'P' ? 'selected' : '' }} value="P">Perempuan</option>
        </select>
    </div>
    <div class="fields">
        <div class="four wide field">
            <label>Tanggal Lahir</label>
            <input type="date" name="birthdate" value="2018-05-07"/>
        </div>
        <div class="six wide field">
            <label>Kategori Umur</label>
            <select name="age_category" class="ui dropdown">
                <option value="">Kategori Umur</option>
                <option {{ (int)@$remaja->age_category === 1 ? 'selected' : ''}} value="1">Sekolah Menengah</option>
                <option {{ (int)@$remaja->age_category === 2 ? 'selected' : ''}} value="2">Vokasional</option>
                <option {{ (int)@$remaja->age_category === 3 ? 'selected' : ''}} value="3">Pra-Staf</option>
                <option {{ (int)@$remaja->age_category === 4 ? 'selected' : ''}} value="4">Staf</option>
            </select>
        </div>
        <div class="six wide field">
            <label>Tempat Sekolah</label>
            <input type="text" name="school" placeholder="Sekolah" value="{{@$remaja->school ?: ''}}">
        </div>
    </div>
    <div class="field">
        <label>Status Perkahwinan</label>
        <select name="marital_status" class="ui dropdown">
            <option value="">Status Perkahwinan</option>
            <option {{ (int)@$remaja->marital_status === 1 ? 'selected' : ''}} value="1">Bujang</option>
            <option {{ (int)@$remaja->marital_status === 2 ? 'selected' : ''}} value="2">Dara</option>
            <option {{ (int)@$remaja->marital_status === 3 ? 'selected' : ''}} value="3">Mono</option>
            <option {{ (int)@$remaja->marital_status === 4 ? 'selected' : ''}} value="4">Poli</option>
            <option {{ (int)@$remaja->marital_status === 5 ? 'selected' : ''}} value="5">Duda</option>
            <option {{ (int)@$remaja->marital_status === 6 ? 'selected' : ''}} value="6">Balu</option>
            <option {{ (int)@$remaja->marital_status === 7 ? 'selected' : ''}} value="7">Janda</option>
        </select>
    </div>
    <label>Jumlah adik-beradik</label>
    <div class="two fields">
        <div class="field">
            <label>Dari Se-ibu</label>
            <input type="text" name="brothers_from_mom" placeholder="Adik Beradik Seibu" value="{{@$remaja->brothers_from_mom ?: ''}}">
        </div>
        <div class="field">
            <label>Dari Semua Ibu</label>
            <input type="text" name="brothers_from_all_mom" placeholder="Adik Beradik Semua Ibu" value="{{@$remaja->brothers_from_all_mom ?: ''}}">
        </div>
    </div>
    <button type="submit" class="ui primary button">{{$submitText}}</button>
</form>

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            zone_id: 'empty',
            kumpulan_id: 'empty',
            original_name: 'empty',
            fullname: 'empty',
            residence_number: 'empty',
            age: ['empty', 'integer'],
            age_category: 'empty',
            school: 'empty',
            marital_status: 'empty',
            brothers_from_mom: ['empty', 'integer'],
            brothers_from_all_mom: ['empty', 'integer'],
        }
    });
});
</script>
@endpush
