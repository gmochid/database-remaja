@extends('layouts.input')

@section('form')

@include('component.remaja-basic-view', ['remaja' => $input])
@include('component.remaja-parents-view', ['remaja' => $input])
@include('component.remaja-history-view', ['remaja' => $input])
@include('component.remaja-assessment-view', ['remaja' => $input])
@include('component.remaja-photo-view', ['remaja' => $input])

<hr/>
<form class="ui form" action="{{route('input.confirm')}}" method="post">
    {{ csrf_field() }} 
    <button type="submit" class="ui primary button">Confirm and Save</button>
</form>
@endsection
