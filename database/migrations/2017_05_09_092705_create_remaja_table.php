<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemajaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remaja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->unsigned();
            $table->integer('kumpulan_id')->unsigned();
            $table->string('original_name');
            $table->string('fullname');
            $table->string('jantina')->nullable();
            $table->string('residence_number')->nullable();
            $table->integer('age')->nullable();
            $table->integer('age_category')->nullable();
            $table->string('school')->nullable();
            $table->integer('marital_status')->nullable();
            $table->integer('brothers_from_mom')->nullable();
            $table->integer('brothers_from_all_mom')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_residence_number')->nullable();
            $table->boolean('father_gisb_member')->nullable();
            $table->string('father_zone_position')->nullable();
            $table->integer('father_marital_status')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mother_residence_number')->nullable();
            $table->boolean('mother_gisb_member')->nullable();
            $table->string('mother_zone_position')->nullable();
            $table->integer('mother_marital_status')->nullable();
            $table->integer('mother_wive_number')->nullable();
            $table->boolean('jenayah1')->nullable();
            $table->boolean('jenayah2')->nullable();
            $table->boolean('jenayah3')->nullable();
            $table->boolean('jenayah4')->nullable();
            $table->boolean('jenayah5')->nullable();
            $table->boolean('jenayah6')->nullable();
            $table->boolean('jenayah7')->nullable();
            $table->boolean('jenayah8')->nullable();
            $table->boolean('jenayah9')->nullable();
            $table->boolean('jenayah10')->nullable();
            $table->boolean('jenayah11')->nullable();
            $table->boolean('jenayah12')->nullable();
            $table->boolean('jenayah13')->nullable();
            $table->boolean('jenayah14')->nullable();
            $table->boolean('jenayah15')->nullable();
            $table->boolean('jenayah16')->nullable();
            $table->boolean('jenayah17')->nullable();
            $table->string('disiplin_sholat')->nullable();
            $table->string('disiplin_pemimpin')->nullable();
            $table->string('disiplin_tugas')->nullable();
            $table->integer('kepemimpinan')->nullable();
            $table->integer('mahmudah_pemurah')->nullable();
            $table->integer('mahmudah_peramah')->nullable();
            $table->integer('mahmudah_ketaatan')->nullable();
            $table->integer('mahmudah_kesetiaan')->nullable();
            $table->integer('mahmudah_tanggung_jawab')->nullable();
            $table->integer('mahmudah_kerajinan')->nullable();
            $table->integer('mahmudah_kejujuran')->nullable();
            $table->integer('mahmudah_ketelusan')->nullable();
            $table->integer('pemimpin_hormat')->nullable();
            $table->integer('pemimpin_rajin')->nullable();
            $table->integer('pemimpin_membesarkan')->nullable();
            $table->integer('keluarga_bapa')->nullable();
            $table->integer('keluarga_ibu')->nullable();
            $table->integer('keluarga_ibu_tiri')->nullable();
            $table->integer('keluarga_adik_beradik')->nullable();
            $table->integer('keluarga_kawan')->nullable();
            $table->integer('keluarga_ahli')->nullable();
            $table->integer('keluarga_masyarakat')->nullable();
            $table->string('kepemimpinan_notes')->nullable();
            $table->string('mahmudah_pemurah_notes')->nullable();
            $table->string('mahmudah_peramah_notes')->nullable();
            $table->string('mahmudah_ketaatan_notes')->nullable();
            $table->string('mahmudah_kesetiaan_notes')->nullable();
            $table->string('mahmudah_tanggung_jawab_notes')->nullable();
            $table->string('mahmudah_kerajinan_notes')->nullable();
            $table->string('mahmudah_kejujuran_notes')->nullable();
            $table->string('mahmudah_ketelusan_notes')->nullable();
            $table->string('pemimpin_hormat_notes')->nullable();
            $table->string('pemimpin_rajin_notes')->nullable();
            $table->string('pemimpin_membesarkan_notes')->nullable();
            $table->string('keluarga_bapa_notes')->nullable();
            $table->string('keluarga_ibu_notes')->nullable();
            $table->string('keluarga_ibu_tiri_notes')->nullable();
            $table->string('keluarga_adik_beradik_notes')->nullable();
            $table->string('keluarga_kawan_notes')->nullable();
            $table->string('keluarga_ahli_notes')->nullable();
            $table->string('keluarga_masyarakat_notes')->nullable();
            $table->string('wawancara')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remaja');
    }
}
