@extends('layouts.input')

@section('form')
@include('component.remaja-parents-edit', [
    'remaja' => $input, 
    'submitText' => 'Next',
    'actionUrl' => route('input.parents.post'),
])
@endsection
