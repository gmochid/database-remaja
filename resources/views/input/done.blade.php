@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div class="ui success message">
                <div class="header">Data saved successfully</div>
                <p>Data telah berhasil disimpan di database.</p>
            </div>
        </div>
    </div>
</div>
@endsection
