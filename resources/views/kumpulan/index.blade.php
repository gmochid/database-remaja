@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">List Kumpulan</div>
                </div>
            </div>
            <div style="margin-top: 16px;">
                <a href="{{ route('kumpulan.create') }}" class="ui basic button">
                    Tambah Kumpulan
                </a>
            </div>
            <table class="ui single line table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Kumpulan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kumpulans as $kumpulan)
                        <tr>
                            <td>{{$kumpulan->id}}</td>
                            <td>{{$kumpulan->name}}</td>
                            <td>
                                <a href="{{route('kumpulan.edit', ['id' => $kumpulan->id])}}" class="ui basic icon button">
                                    <i class="edit icon"></i>
                                </a>
                                <a href="{{route('kumpulan.delete', ['id' => $kumpulan->id])}}" class="ui negative icon button">
                                    <i class="trash icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
