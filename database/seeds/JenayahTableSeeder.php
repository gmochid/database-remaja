<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class JenayahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenayah')->insert([
            ['id' => 1, 'jenayah_name' => 'Merokok'],
            ['id' => 2, 'jenayah_name' => 'Dadah'],
            ['id' => 3, 'jenayah_name' => 'Mencuri barangan ahli jamaah / di premis GISB'],
            ['id' => 4, 'jenayah_name' => 'Mencuri barangan orang luar jamaah / di premis luar'],
            ['id' => 5, 'jenayah_name' => 'Cyber cafe / bermain game'],
            ['id' => 6, 'jenayah_name' => 'Menonton / melayari video lucah di TV atau HP'],
            ['id' => 7, 'jenayah_name' => 'Film dalam negara'],
            ['id' => 8, 'jenayah_name' => 'Film luar negara'],
            ['id' => 9, 'jenayah_name' => 'Lagu-lagu melayu, lagu cinta, dan lagu lain'],
            ['id' => 10, 'jenayah_name' => 'Lagu-lagu bukan melayu'],
            ['id' => 11, 'jenayah_name' => 'Tengok bola di TV / HP'],
            ['id' => 12, 'jenayah_name' => 'Menggunakan Social Media (Facebook, Twitter dan sejenis)'],
            ['id' => 13, 'jenayah_name' => 'Berjumpa / dating'],
            ['id' => 14, 'jenayah_name' => 'Berhubung dengan HP'],
            ['id' => 15, 'jenayah_name' => 'Liwat'],
            ['id' => 16, 'jenayah_name' => 'Lesbian'],
            ['id' => 17, 'jenayah_name' => 'Zina'],
        ]);
    }
}
