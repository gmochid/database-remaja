<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remaja;
use App\Photo;
use App\Zone;
use App\Kumpulan;

class InputController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect()->route('input.basic');
    }

    public function showBasic(Request $request)
    {
        $zones = Zone::all();
        $kumpulans = Kumpulan::all();
        return view('input.basic', [
            'input' => $request->session()->get('input', []),
            'zones' => $zones,
            'kumpulans' => $kumpulans,
        ]);
    }
    
    public function showParents(Request $request)
    {
        return view('input.parents', [
            'input' => $request->session()->get('input', [])
        ]);
    }

    public function showHistory(Request $request)
    {
        return view('input.history', [
            'input' => $request->session()->get('input', [])
        ]);
    }

    public function showAssessment(Request $request)
    {
        return view('input.assessment', [
            'input' => $request->session()->get('input', []),
        ]);
    }

    public function showPhoto(Request $request)
    {
        $sessionData = $request->session()->get('input', []);
        if (!isset($sessionData)) {
            $this->saveObjectToSession($request, array(
                'photos' => array(),
            ));
        } else if(!isset($sessionData->photos)) {
            $this->saveObjectToSession($request, array(
                'photos' => array(),
            ));
        }
        return view('input.photo', [
          'input' => $request->session()->get('input', []),
        ]);
    }

    public function showConfirm(Request $request)
    {
        $input = $request->session()->get('input', []);
        if (!isset($input)) {
            return redirect()->route('input.basic');
        }
        return view('input.confirm', [
            'input' => $request->session()->get('input', []),
        ]);
    }
    
    public function showDone()
    {
        return view('input.done');
    }

    public function postBasic(Request $request)
    {
        $this->saveInputToSession($request);
        return redirect()->route('input.parents');
    }

    public function postParents(Request $request)
    {
        $this->saveInputToSession($request);
        return redirect()->route('input.history');
    }

    public function postHistory(Request $request)
    {
        $this->saveInputToSession($request);
        return redirect()->route('input.assessment');
    }

    public function postAssessment(Request $request)
    {
        $this->saveInputToSession($request);
        return redirect()->route('input.photo');
    }

    public function postPhoto(Request $request)
    {
        $path = $request->file('photo')->store('photos');

        $sessionData = $request->session()->get('input', []);
        $photos = array();
        if ($sessionData != NULL && $sessionData->photos != NULL) {
            $photos = $sessionData->photos;
        }
        array_push($photos, $path);

        $this->saveObjectToSession($request, array(
            'photos' => $photos
        ));
        return redirect()->route('input.confirm');
    }

    public function postDeletePhoto(Request $request)
    {
        $path = $request->input('path');

        $sessionData = $request->session()->get('input', []);
        $photos = array();
        if ($sessionData != NULL && $sessionData->photos != NULL) {
            $photos = $sessionData->photos;
            $photos = array_diff($photos, [$path]);

            $this->saveObjectToSession($request, array(
                'photos' => $photos
            ));
        }
        return redirect()->route('input.photo');
    }

    public function doConfirm(Request $request)
    {
        $input = $request->session()->get('input', []);
        $photos = $input->photos;
        unset($input->photos);

        $remaja = new Remaja();
        $remaja->fill((array)$input);
        $remaja->father_gisb_member = ($remaja->father_gisb_member === 'on') ? true: false;
        $remaja->mother_gisb_member = ($remaja->mother_gisb_member === 'on') ? true: false;
        for($i = 1; $i < 18; $i++)
        {
            $jenayah = 'jenayah' . $i;
            $remaja->$jenayah = ($remaja->$jenayah === 'on') ? true: false;
        }
        $remaja->save();

        foreach($photos as $photo) {
            $photoData = new Photo();
            $photoData->remaja_id = $remaja->id;
            $photoData->path = $photo;
            $photoData->save();
        }

        $request->session()->forget('input');

        return redirect()->route('input.done');
    }

    private function saveInputToSession($request) 
    {
        $input = $request->all();
        $this->saveObjectToSession($request, $input);
    }

    private function saveObjectToSession($request, $object)
    {
        $sessionData = $request->session()->get('input', []);
        $sessionData = (object)array_merge((array) $sessionData, (array) $object);
        $request->session()->put('input', $sessionData);
    }
}
