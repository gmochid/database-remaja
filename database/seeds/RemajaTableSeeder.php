<?php

use Illuminate\Database\Seeder;

class RemajaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('remaja')->insert([
            [
                'id' => 1, 
                'kumpulan_id' => 1,
                'zone_id' => 1,
                'original_name' => 'Bambang',
                'fullname' => 'Adi',
                'residence_number' => 'ABC123',
                'age' => 23,
                'jantina' => 'L',
                'age_category' => 1,
                'school' => 'Wonosobo',
                'marital_status' => 1,
                'brothers_from_mom' => 3,
                'brothers_from_all_mom' => 7,
                'father_name' => 'Dudung',
                'father_residence_number' => 'ABC456',
                'father_gisb_member' => false,
                'father_zone_position' => 'Salatiga',
                'father_marital_status' => 1,
                'mother_name' => 'Ani',
                'mother_residence_number' => 'XYZ456',
                'mother_gisb_member' => true,
                'mother_zone_position' => 'Purwakarta',
                'mother_marital_status' => 1,
                'mother_wive_number' => 1,
                'jenayah1' => true,
                'jenayah2' => true,
                'jenayah3' => false,
                'jenayah4' => false,
                'jenayah5' => false,
                'jenayah6' => false,
                'jenayah7' => false,
                'jenayah8' => false,
                'jenayah9' => false,
                'jenayah10' => false,
                'jenayah11' => false,
                'jenayah12' => false,
                'jenayah13' => false,
                'jenayah14' => false,
                'jenayah15' => false,
                'jenayah16' => false,
                'jenayah17' => false,
                'disiplin_sholat' => '',
                'disiplin_pemimpin' => '',
                'disiplin_tugas' => '',
                'kepemimpinan' => 1,
                'mahmudah_pemurah' => 1,
                'mahmudah_peramah' => 1,
                'mahmudah_ketaatan' => 1,
                'mahmudah_kesetiaan' => 1,
                'mahmudah_tanggung_jawab' => 1,
                'mahmudah_kerajinan' => 1,
                'mahmudah_kejujuran' => 1,
                'mahmudah_ketelusan' => 1,
                'pemimpin_hormat' => 1,
                'pemimpin_rajin' => 1,
                'pemimpin_membesarkan' => 1,
                'keluarga_bapa' => 1,
                'keluarga_ibu' => 1,
                'keluarga_ibu_tiri' => 1,
                'keluarga_adik_beradik' => 1,
                'keluarga_kawan' => 1,
                'keluarga_ahli' => 1,
                'keluarga_masyarakat' => 1,
                'kepemimpinan_notes' => '',
                'mahmudah_pemurah_notes' => '',
                'mahmudah_peramah_notes' => '',
                'mahmudah_ketaatan_notes' => '',
                'mahmudah_kesetiaan_notes' => '',
                'mahmudah_tanggung_jawab_notes' => '',
                'mahmudah_kerajinan_notes' => '',
                'mahmudah_kejujuran_notes' => '',
                'mahmudah_ketelusan_notes' => '',
                'pemimpin_hormat_notes' => '',
                'pemimpin_rajin_notes' => '',
                'pemimpin_membesarkan_notes' => '',
                'keluarga_bapa_notes' => '',
                'keluarga_ibu_notes' => '',
                'keluarga_ibu_tiri_notes' => '',
                'keluarga_adik_beradik_notes' => '',
                'keluarga_kawan_notes' => '',
                'keluarga_ahli_notes' => '',
                'keluarga_masyarakat_notes' => '',
                'wawancara' => 'Baik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
