@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="seven wide column">
            <div class="ui breadcrumb">
                <a href="/" class="section">Home</a>
                <div class="divider"> / </div>
                <a href="{{route('remaja.view', ['id' => $remaja->id, 'section' => $section])}}" class="section">Data Remaja</a>
                <div class="divider"> / </div>
                <div class="active section">Edit Data Remaja</div>
            </div>
        </div>
    </div>
    <div class="ui centered grid">
        <div class="three wide column">
            <div class="ui vertical menu">
                <a 
                    href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => 'basic'])}}" 
                    class="{{$section === 'basic' ? 'active teal' : ''}} item">
                    Biodata Diri</a>
                <a 
                    href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => 'parents'])}}" 
                    class="{{$section === 'parents'  ? 'active teal' : ''}} item">
                    Maklumat Ibu / Bapa</a>
                <a 
                    href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => 'history'])}}" 
                    class="{{$section === 'history'  ? 'active teal' : ''}} item">
                    Rekod Jenayah / Maksiat / Salah Laku</a>
                <a 
                    href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => 'assessment'])}}" 
                    class="{{$section === 'assessment'  ? 'active teal' : ''}} item">
                    Latar Belakang Remaja</a>
                <a 
                    href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => 'photo'])}}" 
                    class="{{$section === 'photo'  ? 'active teal' : ''}} item">
                    Foto</a>
            </div>
            <a href="{{route('home')}}" class="ui basic button">Back</a>
        </div>
        <div class="one wide column"></div>
        <div class="eleven wide column">
            @if($section === 'basic')
            @include('component.remaja-basic-edit', [
                'remaja' => $remaja, 
                'submitText' => 'Save',
                'actionUrl' => $actionUrl,
            ])
            @endif

            @if($section === 'parents')
            @include('component.remaja-parents-edit', [
                'remaja' => $remaja, 
                'submitText' => 'Save',
                'actionUrl' => $actionUrl,
            ])
            @endif

            @if($section === 'history')
            @include('component.remaja-history-edit', [
                'remaja' => $remaja, 
                'submitText' => 'Save',
                'actionUrl' => $actionUrl,
            ])
            @endif

            @if($section === 'assessment')
            @include('component.remaja-assessment-edit', [
                'remaja' => $remaja, 
                'submitText' => 'Save',
                'actionUrl' => $actionUrl,
            ])
            @endif

            @if($section === 'photo')
            @include('component.remaja-photo-edit', [
                'remaja' => $remaja, 
                'submitText' => 'Save',
                'actionUrl' => $actionUrl,
                'actionDeleteUrl' => route('remaja.delete.photo.post', ['id' => $remaja->id]),
            ])
            @endif
        </div>
    </div>
</div>
@endsection
