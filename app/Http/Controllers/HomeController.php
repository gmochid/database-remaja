<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remaja;
use App\Zone;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $zoneId = $request->input('zoneId');
        $search = $request->input('search');
        $orderBy = $request->input('orderBy', 'id');
        $order = $request->input('order', 'asc');

        $remajas = Remaja::search($search, $zoneId)->orderBy($orderBy, $order)->paginate(100);

        return view('home', [
            'remajas' => $remajas->appends($request->except('page')),
            'orderBy' => $orderBy,
            'order' => $order,
            'search' => $search,
            'zones' => Zone::all(),
            'zoneId' => $zoneId,
        ]);
    }

    /**
     * Show the basic data menu
     *
     * @return \Illuminate\Http\Response
     */
    public function basic()
    {
        return view('basic');
    }
}
