@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            @include('component.remaja-basic-view', ['remaja' => $remaja])
            
            <form method="post" action={{route('remaja.delete.post', ['id' => $remaja->id])}}>
                {{ csrf_field() }} 
                <button type="submit" class="ui negative button">Hapus</button>
                <a href="{{route('home')}}" class="ui basic button">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
