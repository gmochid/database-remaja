@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">List Zon</div>
                </div>
            </div>
            <div style="margin-top: 16px;">
                <a href="{{ route('zone.create') }}" class="ui basic button">
                    Tambah Zon
                </a>
            </div>
            <table class="ui single line table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Zon</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($zones as $zone)
                        <tr>
                            <td>{{$zone->id}}</td>
                            <td>{{$zone->name}}</td>
                            <td>
                                <a href="{{route('zone.edit', ['id' => $zone->id])}}" class="ui basic icon button">
                                    <i class="edit icon"></i>
                                </a>
                                <a href="{{route('zone.delete', ['id' => $zone->id])}}" class="ui negative icon button">
                                    <i class="trash icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
