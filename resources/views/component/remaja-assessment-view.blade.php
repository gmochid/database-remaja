<h4 class="ui header">Disiplin</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td class="six wide active">Disiplin Sholat</td>
    <td class="ten wide">{{ @$remaja->disiplin_sholat ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Disiplin Pemimpin</td>
    <td class="ten wide">{{ @$remaja->disiplin_pemimpin ?: '' }}</td>
    </tr>
    <tr>
    <td class="six wide active">Disiplin Tugas</td>
    <td class="ten wide">{{ @$remaja->disiplin_tugas ?: '' }}</td>
    </tr>
</tbody>
</table>
<div class="ui divider"></div>
<h4 class="ui header">Kriteria Kepemimpinan</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td {!! @$remaja->kepemimpinan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Kepemimpinan</td>
    <td class="ten wide">{{ @$remaja->kepemimpinan ? array("", "BERWATAK KEPIMPINAN, ADA TANGGUNGJAWAB, SETIA", "BERWATAK BEKERJA, PENGIKUT, TAAT", "ADA WATAK KEPIMPINAN, TAPI LEBIH MENONJOL SIFAT TAK BAIK, MEMPENGARUHI KAWAN-KAWAN KE ARAH TAK BAIK", "BERMASALAH, SUSAH DENGAR CAKAP, TIADA TANGGUNGJAWAB, TAK MEMPENGARUHI ORANG LAIN")[$remaja->kepemimpinan] : '' }}</td>
    </tr>
    @if(@$remaja->kepemimpinan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->kepemimpinan_notes ?: '' }}</td>
    </tr>
    @endif
</tbody>
</table>
<div class="ui divider"></div>
<h4 class="ui header">Sifat-Sifat Diri Mahmudah / Mazmumah</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td {!! @$remaja->mahmudah_pemurah_notes ? 'rowspan="2"' : '' !!} class="six wide active">Pemurah</td>
    <td class="ten wide">{{ @$remaja->mahmudah_pemurah ? array("", "Terserlah", "Biasa", "Bakhil")[$remaja->mahmudah_pemurah] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_pemurah_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_pemurah_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_peramah_notes ? 'rowspan="2"' : '' !!} class="six wide active">Peramah</td>
    <td class="ten wide">{{ @$remaja->mahmudah_peramah ? array("", "Terserlah", "Biasa", "Tak Mesra")[$remaja->mahmudah_peramah] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_peramah_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_peramah_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_ketaatan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Ketaatan</td>
    <td class="ten wide">{{ @$remaja->mahmudah_ketaatan ? array("", "Terserlah", "Biasa", "Tak Taat")[$remaja->mahmudah_ketaatan] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_ketaatan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_ketaatan_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_kesetiaan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Kesetian</td>
    <td class="ten wide">{{ @$remaja->mahmudah_kesetiaan ? array("", "Terserlah", "Biasa", "Tak Setiap")[$remaja->mahmudah_kesetiaan] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_kesetiaan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_kesetiaan_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_tanggung_jawab_notes ? 'rowspan="2"' : '' !!} class="six wide active">Tanggung Jawab</td>
    <td class="ten wide">{{ @$remaja->mahmudah_tanggung_jawab ? array("", "Bertanggungjawab", "Biasa", "Jarang")[$remaja->mahmudah_tanggung_jawab] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_tanggung_jawab_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_tanggung_jawab_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_kerajinan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Kerajinan</td>
    <td class="ten wide">{{ @$remaja->mahmudah_kerajinan ? array("", "Rajin", "Biasa", "Malas")[$remaja->mahmudah_kerajinan] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_kerajinan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_kerajinan_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_kejujuran_notes ? 'rowspan="2"' : '' !!} class="six wide active">Kejujuran</td>
    <td class="ten wide">{{ @$remaja->mahmudah_kejujuran ? array("", "Jujur", "Biasa", "Tak Jujur")[$remaja->mahmudah_kejujuran] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_kejujuran_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_kejujuran_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->mahmudah_ketelusan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Ketelusan</td>
    <td class="ten wide">{{ @$remaja->mahmudah_ketelusan ? array("", "Telus", "Biasa", "Tak Telus")[$remaja->mahmudah_ketelusan] : '' }}</td>
    </tr>
    @if(@$remaja->mahmudah_ketelusan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->mahmudah_ketelusan_notes ?: '' }}</td>
    </tr>
    @endif
</tbody>
</table>
<div class="ui divider"></div>
<h4 class="ui header">Perhubungan dengan Pemimpin</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td {!! @$remaja->pemimpin_hormat_notes ? 'rowspan="2"' : '' !!} class="six wide active">Hormat</td>
    <td class="ten wide">{{ @$remaja->pemimpin_hormat ? array("", "Hormat", "Biasa", "Tak Sopan")[$remaja->pemimpin_hormat] : '' }}</td>
    </tr>
    @if(@$remaja->pemimpin_hormat_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->pemimpin_hormat_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->pemimpin_rajin_notes ? 'rowspan="2"' : '' !!} class="six wide active">Rajin</td>
    <td class="ten wide">{{ @$remaja->pemimpin_rajin ? array("", "Rajin", "Biasa", "Jarang")[$remaja->pemimpin_rajin] : '' }}</td>
    </tr>
    @if(@$remaja->pemimpin_rajin_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->pemimpin_rajin_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->pemimpin_membesarkan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Membesarkan</td>
    <td class="ten wide">{{ @$remaja->pemimpin_membesarkan ? array("", "Membesarkan Pemimpin", "Biasa", "Tak Pandang Besar pada Pemimpin")[$remaja->pemimpin_membesarkan] : '' }}</td>
    </tr>
    @if(@$remaja->pemimpin_membesarkan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->pemimpin_membesarkan_notes ?: '' }}</td>
    </tr>
    @endif
</tbody>
</table>
<div class="ui divider"></div>
<h4 class="ui header">Perhubungan dengan Keluarga</h4>
<table class="ui green table">
<tbody>
    <tr>
    <td {!! @$remaja->keluarga_bapa_notes ? 'rowspan="2"' : '' !!} class="six wide active">Bapa</td>
    <td class="ten wide">{{ @$remaja->keluarga_bapa ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_bapa] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_bapa_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_bapa_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_ibu_notes ? 'rowspan="2"' : '' !!} class="six wide active">Ibu (Mak Kandung)</td>
    <td class="ten wide">{{ @$remaja->keluarga_ibu  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_ibu] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_ibu_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_ibu_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_ibu_tiri_notes ? 'rowspan="2"' : '' !!} class="six wide active">Ibu (Isteri-Isteri kepada Bapa) selain Ibu Kandung</td>
    <td class="ten wide">{{ @$remaja->keluarga_ibu_tiri  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_ibu_tiri] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_ibu_tiri_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_ibu_tiri_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_adik_beradik_notes ? 'rowspan="2"' : '' !!} class="six wide active">Adik-Beradik</td>
    <td class="ten wide">{{ @$remaja->keluarga_adik_beradik  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_adik_beradik] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_adik_beradik_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_adik_beradik_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_kawan_notes ? 'rowspan="2"' : '' !!} class="six wide active">Kawan-Kawan</td>
    <td class="ten wide">{{ @$remaja->keluarga_kawan  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_kawan] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_kawan_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_kawan_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_ahli_notes ? 'rowspan="2"' : '' !!} class="six wide active">Ahli Jamaah</td>
    <td class="ten wide">{{ @$remaja->keluarga_ahli  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_ahli] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_ahli_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_ahli_notes ?: '' }}</td>
    </tr>
    @endif

    <tr>
    <td {!! @$remaja->keluarga_masyarakat_notes ? 'rowspan="2"' : '' !!} class="six wide active">Masyarakat Luar / Sekitar</td>
    <td class="ten wide">{{ @$remaja->keluarga_masyarakat  ? array("", "Mesra", "Biasa", "Renggang", "Bermasalah")[$remaja->keluarga_masyarakat] : '' }}</td>
    </tr>
    @if(@$remaja->keluarga_masyarakat_notes)
    <tr>
    <td class="disabled ten wide">{{ @$remaja->keluarga_masyarakat_notes ?: '' }}</td>
    </tr>
    @endif
</tbody>
</table>
<div class="ui divider"></div>
<h4 class="ui header">Hasil Wawancara</h4>
<div class="ui form">
    <div class="field">
        <textarea name="wawancara" rows="4">{{@$remaja->wawancara ?: ''}}</textarea>
    </div>
</div>
