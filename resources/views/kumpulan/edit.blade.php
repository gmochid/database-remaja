@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <a href="{{route('kumpulan')}}" class="section">List Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">Edit Kumpulan</div>
                </div>
            </div>
            <div class="ui form" style="margin-top: 16px;">
                <form method="post" action={{route('kumpulan.edit.post', ['id' => $kumpulan->id])}}>
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Id</label>
                        <input type="text" name="name" placeholder="Nama Kumpulan" value="{{ $kumpulan->id }}" disabled/>
                    </div>
                    <div class="field">
                        <label>Nama Kumpulan</label>
                        <input type="text" name="name" placeholder="Nama Kumpulan" value="{{ $kumpulan->name }}"/>
                    </div>
                    <button type="submit" class="ui primary button">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            name: 'empty',
        }
    });
});
</script>
@endpush
