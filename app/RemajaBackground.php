<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemajaBackground extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'remaja_background';
}
