@extends('layouts.input')

@section('form')
@include('component.remaja-assessment-edit', [
    'remaja' => $input, 
    'submitText' => 'Next',
    'actionUrl' => route('input.assessment.post'),
])
@endsection
