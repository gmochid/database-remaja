@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Bani/Kumpulan</a>
                    <i class="right chevron icon divider"></i>
                    <a href="{{route('bani')}}" class="section">List Bani</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">Tambah Bani</div>
                </div>
            </div>
            <div class="ui form" style="margin-top: 16px;">
                <form method="post" action={{route('bani.create.post')}}>
                    {{ csrf_field() }}
                    <div class="field">
                        <label>Nama Bani</label>
                        <input type="text" name="name" placeholder="Nama Bani"/>
                    </div>
                    <button type="submit" class="ui primary button">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            name: 'empty',
        }
    });
});
</script>
@endpush
