@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="seven wide column">
            <div class="ui breadcrumb">
                <a href="/" class="section">Home</a>
                <div class="divider"> / </div>
                <div class="active section">Data Remaja</div>
            </div>
        </div>
    </div>
    <div class="ui centered grid">
        <div class="three wide column">
            <div class="ui vertical menu">
                <a 
                    href="{{route('remaja.view', ['id' => $remaja->id, 'section' => 'basic'])}}" 
                    class="{{$section === 'basic' ? 'active teal' : ''}} item">
                    Biodata Diri</a>
                <a 
                    href="{{route('remaja.view', ['id' => $remaja->id, 'section' => 'parents'])}}" 
                    class="{{$section === 'parents'  ? 'active teal' : ''}} item">
                    Maklumat Ibu / Bapa</a>
                <a 
                    href="{{route('remaja.view', ['id' => $remaja->id, 'section' => 'history'])}}" 
                    class="{{$section === 'history'  ? 'active teal' : ''}} item">
                    Rekod Jenayah / Maksiat / Salah Laku</a>
                <a 
                    href="{{route('remaja.view', ['id' => $remaja->id, 'section' => 'assessment'])}}" 
                    class="{{$section === 'assessment'  ? 'active teal' : ''}} item">
                    Latar Belakang Remaja</a>
                <a 
                    href="{{route('remaja.view', ['id' => $remaja->id, 'section' => 'photo'])}}" 
                    class="{{$section === 'photo'  ? 'active teal' : ''}} item">
                    Foto</a>
            </div>
            <a href="{{route('remaja.edit', ['id' => $remaja->id, 'section' => $section])}}" class="ui primary button">Edit</a>
            <a href="{{route('home')}}" class="ui basic button">Back</a>
        </div>
        <div class="one wide column"></div>
        <div class="eleven wide column">
            @if($section === 'basic')
            @include('component.remaja-basic-view', ['remaja' => $remaja])
            @endif

            @if($section === 'parents')
            @include('component.remaja-parents-view', ['remaja' => $remaja])
            @endif

            @if($section === 'history')
            @include('component.remaja-history-view', ['remaja' => $remaja])
            @endif

            @if($section === 'assessment')
            @include('component.remaja-assessment-view', ['remaja' => $remaja])
            @endif

            @if($section === 'photo')
            @include('component.remaja-photo-view', ['remaja' => $remaja])
            @endif
        </div>
    </div>
</div>
@endsection
