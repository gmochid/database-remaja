@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <h2 class="ui header">Input Database Remaja</h2>
            <div class="ui big breadcrumb">
                @if(Route::currentRouteName() === 'input.basic')
                    <div class="active section">Biodata Diri</div>
                @else
                    <a href="{{route('input.basic')}}" class="section">Biodata Diri</a>
                @endif
                <i class="right chevron icon divider"></i>
                
                @if(Route::currentRouteName() === 'input.parents')
                    <div class="active section">Maklumat Ibu/Bapa</div>
                @else
                    <a href="{{route('input.parents')}}" class="section">Maklumat Ibu/Bapa</a>
                @endif
                <i class="right chevron icon divider"></i>

                @if(Route::currentRouteName() === 'input.history')
                    <div class="active section">Rekod Jenayah / Maksiat / Salah Laku</div>
                @else
                    <a href="{{route('input.history')}}" class="section">Rekod Jenayah / Maksiat / Salah Laku</a>
                @endif
                <i class="right chevron icon divider"></i>

                @if(Route::currentRouteName() === 'input.assessment')
                    <div class="active section">Latar Belakang</div>
                @else
                    <a href="{{route('input.assessment')}}" class="section">Latar Belakang</a>
                @endif
                <i class="right chevron icon divider"></i>

                @if(Route::currentRouteName() === 'input.photo')
                    <div class="active section">Foto</div>
                @else
                    <a href="{{route('input.photo')}}" class="section">Foto</a>
                @endif
                <i class="right chevron icon divider"></i>
                
                @if(Route::currentRouteName() === 'input.confirm')
                    <div class="active section">Konfirmasi</div>
                @else
                    <a href="{{route('input.confirm')}}" class="section">Konfirmasi</a>
                @endif
                <i class="right chevron icon divider"></i>

                @if(Route::currentRouteName() === 'input.done')
                    <div class="active section">Selesai</div>
                @else
                    <div class="section" style="color:#D3D3D3">Selesai</div>
                @endif
            </div>
            <div class="ui section divider"></div>
            @if(Route::currentRouteName() !== 'input.done')
            <div class="ui warning message">
                Data tidak akan tersimpan di database bila belum memasuki fase <b>Selesai</b>
            </div>
            @endif
            @yield('form')
        </div>
    </div>
</div>
@endsection
