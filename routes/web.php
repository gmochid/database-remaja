<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/input', 'InputController@index')->name('input');
Route::get('/input/basic', 'InputController@showBasic')->name('input.basic');
Route::post('/input/basic', 'InputController@postBasic')->name('input.basic.post');
Route::get('/input/parents', 'InputController@showParents')->name('input.parents');
Route::post('/input/parents', 'InputController@postParents')->name('input.parents.post');
Route::get('/input/history', 'InputController@showHistory')->name('input.history');
Route::post('/input/history', 'InputController@postHistory')->name('input.history.post');
Route::get('/input/assessment', 'InputController@showAssessment')->name('input.assessment');
Route::post('/input/assessment', 'InputController@postAssessment')->name('input.assessment.post');
Route::get('/input/photo', 'InputController@showPhoto')->name('input.photo');
Route::post('/input/photo', 'InputController@postPhoto')->name('input.photo.post');
Route::post('/input/photo/delete', 'InputController@postDeletePhoto')->name('input.photo.delete.post');
Route::get('/input/confirm', 'InputController@showConfirm')->name('input.confirm');
Route::post('/input/confirm', 'InputController@doConfirm')->name('input.confirm.post');
Route::get('/input/done', 'InputController@showDone')->name('input.done');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/remaja/view/{id}', 'RemajaController@view')->name('remaja.view');
Route::get('/remaja/edit/{id}', 'RemajaController@showEdit')->name('remaja.edit');
Route::post('/remaja/edit/{id}', 'RemajaController@doEdit')->name('remaja.edit.post');
Route::get('/remaja/delete/{id}', 'RemajaController@showDelete')->name('remaja.delete');
Route::post('/remaja/delete/{id}', 'RemajaController@doDelete')->name('remaja.delete.post');
Route::post('/remaja/delete/{id}/photo', 'RemajaController@doDeletePhoto')->name('remaja.delete.photo.post');

Route::get('/basic', 'HomeController@basic')->name('basic');

Route::get('/zone', 'ZoneController@index')->name('zone');
Route::get('/zone/create', 'ZoneController@showCreate')->name('zone.create');
Route::get('/zone/edit/{id}', 'ZoneController@showEdit')->name('zone.edit');
Route::get('/zone/delete/{id}', 'ZoneController@showDelete')->name('zone.delete');
Route::post('/zone/create', 'ZoneController@doCreate')->name('zone.create.post');
Route::post('/zone/edit/{id}', 'ZoneController@doEdit')->name('zone.edit.post');
Route::post('/zone/delete/{id}', 'ZoneController@doDelete')->name('zone.delete.post');

Route::get('/bani', 'BaniController@index')->name('bani');
Route::get('/bani/create', 'BaniController@showCreate')->name('bani.create');
Route::get('/bani/edit/{id}', 'BaniController@showEdit')->name('bani.edit');
Route::get('/bani/delete/{id}', 'BaniController@showDelete')->name('bani.delete');
Route::post('/bani/create', 'BaniController@doCreate')->name('bani.create.post');
Route::post('/bani/edit/{id}', 'BaniController@doEdit')->name('bani.edit.post');
Route::post('/bani/delete/{id}', 'BaniController@doDelete')->name('bani.delete.post');

Route::get('/kumpulan', 'KumpulanController@index')->name('kumpulan');
Route::get('/kumpulan/create', 'KumpulanController@showCreate')->name('kumpulan.create');
Route::get('/kumpulan/edit/{id}', 'KumpulanController@showEdit')->name('kumpulan.edit');
Route::get('/kumpulan/delete/{id}', 'KumpulanController@showDelete')->name('kumpulan.delete');
Route::post('/kumpulan/create', 'KumpulanController@doCreate')->name('kumpulan.create.post');
Route::post('/kumpulan/edit/{id}', 'KumpulanController@doEdit')->name('kumpulan.edit.post');
Route::post('/kumpulan/delete/{id}', 'KumpulanController@doDelete')->name('kumpulan.delete.post');
