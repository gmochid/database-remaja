<form class="ui form" action="{{ $actionUrl }}" method="post">
    {{ csrf_field() }} 

    <h4 class="ui header">Disiplin</h4>
    <div class="field">
        <label>Disiplin Sholat</label>
        <input type="text" name="disiplin_sholat" value="{{@$remaja->disiplin_sholat ?: ''}}">
    </div>
    <div class="field">
        <label>Disiplin Pemimpin</label>
        <input type="text" name="disiplin_pemimpin" value="{{@$remaja->disiplin_pemimpin ?: ''}}">
    </div>
    <div class="field">
        <label>Disiplin Tugas</label>
        <input type="text" name="disiplin_tugas" value="{{@$remaja->disiplin_tugas ?: ''}}">
    </div>
    <div class="ui divider"></div>

    <h4 class="ui header">Kriteria Kepemimpinan</h4>
    <div class="grouped fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="kepemimpinan" value="1" {{ (int)@$remaja->kepemimpinan === 1 ? 'checked' : ''}} >
            <label>BERWATAK KEPIMPINAN, ADA TANGGUNGJAWAB, SETIA</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="kepemimpinan" value="2" {{ (int)@$remaja->kepemimpinan === 2 ? 'checked' : ''}} >
            <label>BERWATAK BEKERJA, PENGIKUT, TAAT</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="kepemimpinan" value="3" {{ (int)@$remaja->kepemimpinan === 3 ? 'checked' : ''}} >
            <label>ADA WATAK KEPIMPINAN, TAPI LEBIH MENONJOL SIFAT TAK BAIK, MEMPENGARUHI KAWAN-KAWAN KE ARAH TAK BAIK</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="kepemimpinan" value="4" {{ (int)@$remaja->kepemimpinan === 4 ? 'checked' : ''}} >
            <label>BERMASALAH, SUSAH DENGAR CAKAP, TIADA TANGGUNGJAWAB, TAK MEMPENGARUHI ORANG LAIN</label>
        </div>
        </div>
    </div>
    <div class="field">
        <label>Catatan</label>
        <input type="text" name="kepemimpinan_notes" value="{{@$remaja->kepemimpinan_notes ?: ''}}">
    </div>
    <div class="ui divider"></div>

    <h4 class="ui header">Sifat-Sifat Diri Mahmudah / Mazmumah</h4>
    <label>Pemurah</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_pemurah" value="1" {{ (int)@$remaja->mahmudah_pemurah === 1 ? 'checked' : ''}} >
            <label>Terserlah</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_pemurah" value="2" {{ (int)@$remaja->mahmudah_pemurah === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_pemurah" value="3" {{ (int)@$remaja->mahmudah_pemurah === 3 ? 'checked' : ''}} >
            <label>Bakhil</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_pemurah_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_pemurah_notes ?: ''}}">
        </div>
    </div>

    <label>Peramah</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_peramah" value="1" {{ (int)@$remaja->mahmudah_peramah === 1 ? 'checked' : ''}} >
            <label>Terserlah</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_peramah" value="2" {{ (int)@$remaja->mahmudah_peramah === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_peramah" value="3" {{ (int)@$remaja->mahmudah_peramah === 3 ? 'checked' : ''}} >
            <label>Tak Mesra</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_peramah_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_peramah_notes ?: ''}}">
        </div>
    </div>

    <label>Ketaatan</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketaatan" value="1" {{ (int)@$remaja->mahmudah_ketaatan === 1 ? 'checked' : ''}} >
            <label>Terserlah</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketaatan" value="2" {{ (int)@$remaja->mahmudah_ketaatan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketaatan" value="3" {{ (int)@$remaja->mahmudah_ketaatan === 3 ? 'checked' : ''}} >
            <label>Tak Taat</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_ketaatan_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_ketaatan_notes ?: ''}}">
        </div>
    </div>

    <label>Kesetiaan</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kesetiaan" value="1" {{ (int)@$remaja->mahmudah_kesetiaan === 1 ? 'checked' : ''}} >
            <label>Terserlah</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kesetiaan" value="2" {{ (int)@$remaja->mahmudah_kesetiaan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kesetiaan" value="3" {{ (int)@$remaja->mahmudah_kesetiaan === 3 ? 'checked' : ''}} >
            <label>Tak Setia</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_kesetiaan_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_kesetiaan_notes ?: ''}}">
        </div>
    </div>

    <label>Tanggung Jawab</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_tanggung_jawab" value="1" {{ (int)@$remaja->mahmudah_tanggung_jawab === 1 ? 'checked' : ''}} >
            <label>Bertanggungjawab</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_tanggung_jawab" value="2" {{ (int)@$remaja->mahmudah_tanggung_jawab === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_tanggung_jawab" value="3" {{ (int)@$remaja->mahmudah_tanggung_jawab === 3 ? 'checked' : ''}} >
            <label>Tak Bertanggungjawab</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_tanggung_jawab_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_tanggung_jawab_notes ?: ''}}">
        </div>
    </div>

    <label>Kerajinan</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kerajinan" value="1" {{ (int)@$remaja->mahmudah_kerajinan === 1 ? 'checked' : ''}} >
            <label>Rajin</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kerajinan" value="2" {{ (int)@$remaja->mahmudah_kerajinan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kerajinan" value="3" {{ (int)@$remaja->mahmudah_kerajinan === 3 ? 'checked' : ''}} >
            <label>Malas</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_kerajinan_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_kerajinan_notes ?: ''}}">
        </div>
    </div>

    <label>Kejujuran</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kejujuran" value="1" {{ (int)@$remaja->mahmudah_kejujuran === 1 ? 'checked' : ''}} >
            <label>Jujur</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kejujuran" value="2" {{ (int)@$remaja->mahmudah_kejujuran === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_kejujuran" value="3" {{ (int)@$remaja->mahmudah_kejujuran === 3 ? 'checked' : ''}} >
            <label>Tak Jujur</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_kejujuran_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_kejujuran_notes ?: ''}}">
        </div>
    </div>

    <label>Ketelusan</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketelusan" value="1" {{ (int)@$remaja->mahmudah_ketelusan === 1 ? 'checked' : ''}} >
            <label>Telus</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketelusan" value="2" {{ (int)@$remaja->mahmudah_ketelusan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="mahmudah_ketelusan" value="3" {{ (int)@$remaja->mahmudah_ketelusan === 3 ? 'checked' : ''}} >
            <label>Tak Telus</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="mahmudah_ketelusan_notes" placeholder="Catatan" value="{{@$remaja->mahmudah_ketelusan_notes ?: ''}}">
        </div>
    </div>

    <div class="ui divider"></div>
    <h4 class="ui header">Perhubungan dengan Pemimpin</h4>
    <label>Hormat (beradab)</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_hormat" value="1" {{ (int)@$remaja->pemimpin_hormat === 1 ? 'checked' : ''}} >
            <label>Hormat</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_hormat" value="2" {{ (int)@$remaja->pemimpin_hormat === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_hormat" value="3" {{ (int)@$remaja->pemimpin_hormat === 3 ? 'checked' : ''}} >
            <label>Tak Sopan</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="pemimpin_hormat_notes" placeholder="Catatan" value="{{@$remaja->pemimpin_hormat_notes ?: ''}}">
        </div>
    </div>

    <label>Rajin Berhubung</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_rajin" value="1" {{ (int)@$remaja->pemimpin_rajin === 1 ? 'checked' : ''}} >
            <label>Rajin</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_rajin" value="2" {{ (int)@$remaja->pemimpin_rajin === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_rajin" value="3" {{ (int)@$remaja->pemimpin_rajin === 3 ? 'checked' : ''}} >
            <label>Jarang</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="pemimpin_rajin_notes" placeholder="Catatan" value="{{@$remaja->pemimpin_rajin_notes ?: ''}}">
        </div>
    </div>

    <label>Membesarkan Pemimpin</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_membesarkan" value="1" {{ (int)@$remaja->pemimpin_membesarkan === 1 ? 'checked' : ''}} >
            <label>Membesarkan Pemimpin</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_membesarkan" value="2" {{ (int)@$remaja->pemimpin_membesarkan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="pemimpin_membesarkan" value="3" {{ (int)@$remaja->pemimpin_membesarkan === 3 ? 'checked' : ''}} >
            <label>Tak Pandang Besar pada Pemimpin</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="pemimpin_membesarkan_notes" placeholder="Catatan" value="{{@$remaja->pemimpin_membesarkan_notes ?: ''}}">
        </div>
    </div>
    <div class="ui divider"></div>

    <h4 class="ui header">Perhubungan dengan Keluarga</h4>
    <label>Bapa</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_bapa" value="1" {{ (int)@$remaja->keluarga_bapa === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_bapa" value="2" {{ (int)@$remaja->keluarga_bapa === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_bapa" value="3" {{ (int)@$remaja->keluarga_bapa === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_bapa_notes" placeholder="Catatan" value="{{@$remaja->keluarga_bapa_notes ?: ''}}">
        </div>
    </div>

    <label>Ibu (Mak Kandung)</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu" value="1" {{ (int)@$remaja->keluarga_ibu === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu" value="2" {{ (int)@$remaja->keluarga_ibu === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu" value="3" {{ (int)@$remaja->keluarga_ibu === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_ibu_notes" placeholder="Catatan" value="{{@$remaja->keluarga_ibu_notes ?: ''}}">
        </div>
    </div>

    <label>Ibu (Isteri-Isteri kepada Bapa) selain Ibu Kandung</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu_tiri" value="1" {{ (int)@$remaja->keluarga_ibu_tiri === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu_tiri" value="2" {{ (int)@$remaja->keluarga_ibu_tiri === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu_tiri" value="3" {{ (int)@$remaja->keluarga_ibu_tiri === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ibu_tiri" value="4" {{ (int)@$remaja->keluarga_ibu_tiri === 4 ? 'checked' : ''}} >
            <label>Bermasalah</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_ibu_tiri_notes" placeholder="Catatan" value="{{@$remaja->keluarga_ibu_tiri_notes ?: ''}}">
        </div>
    </div>

    <label>Adik-Beradik</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_adik_beradik" value="1" {{ (int)@$remaja->keluarga_adik_beradik === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_adik_beradik" value="2" {{ (int)@$remaja->keluarga_adik_beradik === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_adik_beradik" value="3" {{ (int)@$remaja->keluarga_adik_beradik === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_adik_beradik" value="4" {{ (int)@$remaja->keluarga_adik_beradik === 4 ? 'checked' : ''}} >
            <label>Bermasalah</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_adik_beradik_notes" placeholder="Catatan" value="{{@$remaja->keluarga_adik_beradik_notes ?: ''}}">
        </div>
    </div>

    <label>Kawan-Kawan</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_kawan" value="1" {{ (int)@$remaja->keluarga_kawan === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_kawan" value="2" {{ (int)@$remaja->keluarga_kawan === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_kawan" value="3" {{ (int)@$remaja->keluarga_kawan === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_kawan" value="4" {{ (int)@$remaja->keluarga_kawan === 4 ? 'checked' : ''}} >
            <label>Bermasalah</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_kawan_notes" placeholder="Catatan" value="{{@$remaja->keluarga_kawan_notes ?: ''}}">
        </div>
    </div>

    <label>Ahli Jamaah</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ahli" value="1" {{ (int)@$remaja->keluarga_ahli === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ahli" value="2" {{ (int)@$remaja->keluarga_ahli === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ahli" value="3" {{ (int)@$remaja->keluarga_ahli === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_ahli" value="4" {{ (int)@$remaja->keluarga_ahli === 4 ? 'checked' : ''}} >
            <label>Bermasalah</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_ahli_notes" placeholder="Catatan" value="{{@$remaja->keluarga_ahli_notes ?: ''}}">
        </div>
    </div>

    <label>Masyarakat Luar / Sekitar</label>
    <div class="inline fields">
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_masyarakat" value="1" {{ (int)@$remaja->keluarga_masyarakat === 1 ? 'checked' : ''}} >
            <label>Mesra</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_masyarakat" value="2" {{ (int)@$remaja->keluarga_masyarakat === 2 ? 'checked' : ''}} >
            <label>Biasa</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_masyarakat" value="3" {{ (int)@$remaja->keluarga_masyarakat === 3 ? 'checked' : ''}} >
            <label>Renggang</label>
        </div>
        </div>
        <div class="field">
        <div class="ui radio checkbox">
            <input type="radio" name="keluarga_masyarakat" value="4" {{ (int)@$remaja->keluarga_masyarakat === 4 ? 'checked' : ''}} >
            <label>Bermasalah</label>
        </div>
        </div>
        <div class="field">
            <input type="text" name="keluarga_masyarakat_notes" placeholder="Catatan" value="{{@$remaja->keluarga_masyarakat_notes ?: ''}}">
        </div>
    </div>

    <h4 class="ui header">Hasil Wawancara</h4>
    <div class="field">
        <textarea name="wawancara" rows="4">{{@$remaja->wawancara ?: ''}}</textarea>
    </div>

    <button type="submit" class="ui primary button">{{$submitText}}</button>
</form>

@push('js')
<script>
$(document).ready(function(){
    $('.ui.form').form({
        fields: {
            disiplin_sholat: 'empty',
            disiplin_pemimpin: 'empty',
            disiplin_tugas: 'empty',
            kepemimpinan: 'checked',
            mahmudah_pemurah: 'checked',
            mahmudah_peramah: 'checked',
            mahmudah_ketaatan: 'checked',
            mahmudah_kesetiaan: 'checked',
            mahmudah_tanggung_jawab: 'checked',
            mahmudah_kerajinan: 'checked',
            mahmudah_kejujuran: 'checked',
            mahmudah_ketelusan: 'checked',
            pemimpin_hormat: 'checked',
            pemimpin_rajin: 'checked',
            pemimpin_membesarkan: 'checked',
            keluarga_bapa: 'checked',
            keluarga_ibu: 'checked',
            keluarga_ibu_tiri: 'checked',
            keluarga_adik_beradik: 'checked',
            keluarga_kawan: 'checked',
            keluarga_ahli: 'checked',
            keluarga_masyarakat: 'checked',
        }
    });
});
</script>
@endpush
