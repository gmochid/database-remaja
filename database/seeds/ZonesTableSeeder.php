<?php

use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zones')->insert([
            ['id' => 1, 'name' => 'UTARA'],
            ['id' => 2, 'name' => 'KEDAH'],
            ['id' => 3, 'name' => 'PKBM'],
            ['id' => 4, 'name' => 'PENANG'],
            ['id' => 5, 'name' => 'PERAK 1'],
            ['id' => 6, 'name' => 'PERAK 2'],
            ['id' => 7, 'name' => 'SELANGOR 1'],
            ['id' => 8, 'name' => 'SELANGOR 2'],
            ['id' => 9, 'name' => 'PUSAT'],
            ['id' => 10, 'name' => 'SELANGOR 3'],
            ['id' => 11, 'name' => 'TENGAH'],
            ['id' => 12, 'name' => 'UTAMA'],
            ['id' => 13, 'name' => 'EKSEKUTIF'],
            ['id' => 14, 'name' => 'YAYASAN'],
            ['id' => 15, 'name' => 'NEG SEMBILAN'],
            ['id' => 16, 'name' => 'MELAKA'],
            ['id' => 17, 'name' => 'JOHOR 1'],
            ['id' => 18, 'name' => 'JOHOR 2'],
            ['id' => 19, 'name' => 'PAHANG'],
            ['id' => 20, 'name' => 'PAHANG 2'],
            ['id' => 21, 'name' => 'TERENGGANU'],
            ['id' => 22, 'name' => 'KELANTAN'],
            ['id' => 23, 'name' => 'SABAH 1/3'],
            ['id' => 24, 'name' => 'SABAH 2/4'],
            ['id' => 25, 'name' => 'SARAWAK 1'],
            ['id' => 26, 'name' => 'SARAWAK 2'],
            ['id' => 27, 'name' => 'LABUAN'],
            ['id' => 28, 'name' => 'THAI 1 (PHUKET/SATUN)'],
            ['id' => 29, 'name' => 'THAI 2 (BANGKOK)'],
            ['id' => 30, 'name' => 'THAI 3 (CHIANG MAI)'],
            ['id' => 31, 'name' => 'PENDIDIKAN INSANIAH'],
            ['id' => 32, 'name' => 'PENANG 2'],
            ['id' => 33, 'name' => 'AUSTRALIA TIMUR'],
            ['id' => 34, 'name' => 'AUSTRALIA BARAT'],
            ['id' => 35, 'name' => 'TURKI'],
            ['id' => 36, 'name' => 'SUMATERA 1/5'],
            ['id' => 37, 'name' => 'SUMATERA 2'],
            ['id' => 38, 'name' => 'SUMATERA 3'],
            ['id' => 39, 'name' => 'JAWA 1'],
            ['id' => 40, 'name' => 'JAWA 3'],
            ['id' => 41, 'name' => 'MAKASSAR'],
            ['id' => 42, 'name' => 'PAPUA'],
            ['id' => 43, 'name' => 'SYAM'],
            ['id' => 44, 'name' => 'HARAMAIN'],
            ['id' => 45, 'name' => 'MESIR'],
        ]);
    }
}
