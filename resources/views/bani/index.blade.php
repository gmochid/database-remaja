@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui centered grid">
        <div class="fourteen wide column">
            <div>
                <div class="ui breadcrumb">
                    <a href="{{route('basic')}}" class="section">Data Zon/Kawasan</a>
                    <i class="right chevron icon divider"></i>
                    <div class="active section">List Bani</div>
                </div>
            </div>
            <div style="margin-top: 16px;">
                <a href="{{ route('bani.create') }}" class="ui basic button">
                    Tambah Bani
                </a>
            </div>
            <table class="ui single line table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Bani</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($banis as $bani)
                        <tr>
                            <td>{{$bani->id}}</td>
                            <td>{{$bani->name}}</td>
                            <td>
                                <a href="{{route('bani.edit', ['id' => $bani->id])}}" class="ui basic icon button">
                                    <i class="edit icon"></i>
                                </a>
                                <a href="{{route('bani.delete', ['id' => $bani->id])}}" class="ui negative icon button">
                                    <i class="trash icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
