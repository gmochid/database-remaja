<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remaja extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'remaja';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['section', '_token'];

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function kumpulan()
    {
        return $this->belongsTo('App\Kumpulan');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function scopeSearch($query, $keywords, $zoneId)
    {
        return $query->when($zoneId, function($query) use ($zoneId) {
                            return $query->where('zone_id', $zoneId);
                        })
                     ->when($keywords, function($query) use ($keywords) {
                            return $query->where('fullname', 'LIKE', '%'.$keywords.'%');
                        });
    }
}
