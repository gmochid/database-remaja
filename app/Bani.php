<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bani extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bani';

    public function kumpulan()
    {
        return $this->hasMany('App\Kumpulan');
    }
}
